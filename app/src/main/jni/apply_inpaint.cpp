
#include "inpainting.h"
using namespace std;
using namespace cv;


//****************************************************************************//

/* 1st and 2nd derivative of 1D gaussian
*/
void getGaussianDerivs(double sigma, int M, vector<double>& gaussian, vector<double>& dg, vector<double>& d2g) {
	int L = (M - 1) / 2;
	double sigma_sq = sigma * sigma;
	double sigma_quad = sigma_sq*sigma_sq;
	dg.resize(M); d2g.resize(M); gaussian.resize(M);

	Mat_<double> g = getGaussianKernel(M, sigma, CV_64F);
	for (double i = -L; i < L + 1.0; i += 1.0) {
		int idx = (int)(i + L);
		gaussian[idx] = g(idx);
		//
		dg[idx] = (-i / sigma_sq) * g(idx);
		d2g[idx] = (-sigma_sq + i*i) / sigma_quad * g(idx);
	}
}

/* 1st and 2nd derivative of smoothed curve point */
void getdX(vector<double> x,
	int n,
	double sigma,
	double& gx,
	double& dgx,
	double& d2gx,
	vector<double> g,
	vector<double> dg,
	vector<double> d2g,
	bool isOpen = false)
{
	int L = (g.size() - 1) / 2;

	gx = dgx = d2gx = 0.0;
	for (int k = -L; k < L + 1; k++) {
		double x_n_k;
		if (n - k < 0) {
			if (isOpen) {
				//open curve - mirror values on border
				x_n_k = x[-(n - k)];
			}
			else {
				//closed curve - take values from end of curve
				x_n_k = x[x.size() + (n - k)];
			}
		}
		else if (n - k > x.size() - 1) {
			if (isOpen) {
				//mirror value on border
				x_n_k = x[n + k];
			}
			else {
				x_n_k = x[(n - k) - (x.size())];
			}
		}
		else {
			//          cout << n-k;
			x_n_k = x[n - k];
		}
		//      cout << "* g[" << g[k + L] << "], ";

		gx += x_n_k * g[k + L]; //gaussians go [0 -> M-1]
		dgx += x_n_k * dg[k + L];
		d2gx += x_n_k * d2g[k + L];
	}
	//  cout << endl;
}


/* 0th, 1st and 2nd derivatives of whole smoothed curve */
void getdXcurve(vector<double> x,
	double sigma,
	vector<double>& gx,
	vector<double>& dx,
	vector<double>& d2x,
	vector<double> g,
	vector<double> dg,
	vector<double> d2g,
	bool isOpen = false)
{
	gx.resize(x.size());
	dx.resize(x.size());
	d2x.resize(x.size());
	for (int i = 0; i<x.size(); i++) {
		double gausx, dgx, d2gx; getdX(x, i, sigma, gausx, dgx, d2gx, g, dg, d2g, isOpen);
		gx[i] = gausx;
		dx[i] = dgx;
		d2x[i] = d2gx;
	}
}

template<typename T, typename V>
void PolyLineSplit(const vector<Point_<T> >& pl, vector<V>& contourx, vector<V>& contoury) {
	contourx.resize(pl.size());
	contoury.resize(pl.size());

	for (int j = 0; j<pl.size(); j++)
	{
		contourx[j] = (V)(pl[j].x);
		contoury[j] = (V)(pl[j].y);
	}
}

template<typename T, typename V>
void PolyLineMerge(vector<Point_<T> >& pl, const vector<V>& contourx, const vector<V>& contoury) {
	assert(contourx.size() == contoury.size());
	pl.resize(contourx.size());
	for (int j = 0; j<contourx.size(); j++) {
		pl[j].x = (T)(contourx[j]);
		pl[j].y = (T)(contoury[j]);
	}
}


void smoothContours(vector<vector<Point> > cnts_in, vector<vector<Point> > &cnts_out)
{
	for (int i = 0; i < cnts_in.size(); i++)
	{
		vector<Point> curve = cnts_in[i];

		double sigma = 3.0;
		int M = cvRound((10.0*sigma + 1.0) / 2.0) * 2 - 1;
		assert(M % 2 == 1); //M is an odd number

		//create kernels
		vector<double> g, dg, d2g; getGaussianDerivs(sigma, M, g, dg, d2g);

		vector<double> curvex, curvey, smoothx, smoothy;
		PolyLineSplit(curve, curvex, curvey);

		bool isOpen = false;

		vector<double> X, XX, Y, YY;
		getdXcurve(curvex, sigma, smoothx, X, XX, g, dg, d2g, isOpen);
		getdXcurve(curvey, sigma, smoothy, Y, YY, g, dg, d2g, isOpen);

		PolyLineMerge(curve, smoothx, smoothy);

		vector<Point> tmp_smooth_cnts;

		for (int j = 0; j < smoothx.size(); j++)
		{
			Point tmp = Point(smoothx[j], smoothy[j]);
			tmp_smooth_cnts.push_back(tmp);
		}

		cnts_out.push_back(tmp_smooth_cnts);
	}

}




//***************************************************************************//


void findBestMatchPatch(int sz, Mat mask, Mat searchMask, Mat img_cielab, Point pmax, Point &q_best_match, bool debuging)
{
	vector<Point> best_match_list;			// list of best matched patches. All SSD values the same
	float ssd = (float)(255 * sz*sz * 3);	//max possible value of ssd
	Point pt_max_ssd;

	int r = mask.rows;
	int c = mask.cols;
	int ab = (sz - 1) / 2;
	int count = 0;

	for (int i = ab; i < c - ab; i++)  // along x   i = c - 6    cols -- 0, 1, 2, ... c-1     i = 0; i < 5 -- 0, 1, 2, 3, 4
	{
		for (int j = ab; j < r - ab; j++)  // along y  j = r - 6
		{
			Rect patch_qrect = Rect(i - ab, j - ab, sz, sz);   // (c - 10, r - 10) ... (c - 1, r - 1)

			if ((int)sum(searchMask(patch_qrect))[0] != 255 * sz*sz)  // patch chosen from the masked area -- reject
				continue;

			float temp_ssd = 0; // sqrt(sum(diff)[0]); // square root of sum of squares of differences

			// loop through patch at q
			for (int k = -1 * ab; k < ab + 1; k++) //x
			{
				for (int t = -1 * ab; t < ab + 1; t++) //y
				{
					if (pmax.y + t > r - 1 || pmax.y + t < 0 || pmax.x + k > c - 1 || pmax.x + k < 0)
						continue;

					if (mask.at<uchar>(pmax.y + t, pmax.x + k) != 0 && mask.at<uchar>(j + t, i + k) != 0)
					{
						float del1 = img_cielab.at<Vec3b>(pmax.y + t, pmax.x + k)[0] - img_cielab.at<Vec3b>(j + t, i + k)[0];
						float del2 = img_cielab.at<Vec3b>(pmax.y + t, pmax.x + k)[1] - img_cielab.at<Vec3b>(j + t, i + k)[1];
						float del3 = img_cielab.at<Vec3b>(pmax.y + t, pmax.x + k)[2] - img_cielab.at<Vec3b>(j + t, i + k)[2];

						temp_ssd += pow(del1, 2) + pow(del2, 2) + pow(del3, 2);
					}
				}
			}

			count++;
			temp_ssd = sqrt(temp_ssd);

			if (temp_ssd < ssd)  // get patch of min ssd
			{
				best_match_list.clear();
				ssd = temp_ssd;
				best_match_list.push_back(Point(i, j));
			}
			else if (temp_ssd == ssd)
			{
				best_match_list.push_back(Point(i, j));
			}

		}
	}
	if (best_match_list.size() > 1)
	{
		float dist = 0;
		float shortest_dist = r*c;
		for (int bst_list = 0; bst_list < best_match_list.size(); bst_list++)
		{
			dist = sqrt(pow(best_match_list[bst_list].x - pmax.x, 2) + pow(best_match_list[bst_list].y - pmax.y, 2));
			if (dist < shortest_dist)
			{
				shortest_dist = dist;
				q_best_match = best_match_list[bst_list];
			}
		}
	}
	else
		q_best_match = best_match_list[0];

	if (debuging == true)
		cout << "best ssd value found: " << ssd << " from list of " << best_match_list.size() << endl;

}

void compNbySobel(Mat mask, Point p, int a, Mat &nMat)
{
	//int a = (int)floor((double)sz/2);

	int scale = 1;
	int delta = 0;
	int ddepth = CV_32F;

	Mat np_x, np_y;

	Mat tmpMask;
	mask.convertTo(tmpMask, CV_32F);

	/// Gradient X
	Sobel(tmpMask, np_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);

	/// Gradient Y
	Sobel(tmpMask, np_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);

	//converting np to unit vector by dividing by sqrt(np_x^2 + np_y^2)
	float npx = -1 * np_x.at<float>(a, a);  //gradient is always computed from black to white so reversing it
	float npy = -1 * np_y.at<float>(a, a);

	float np = sqrt(npx*npx + npy*npy);

	if (np == 0)
	{
		nMat.at<Vec2f>(p)[0] = 0;
		nMat.at<Vec2f>(p)[1] = 0;
	}
	else
	{
		nMat.at<Vec2f>(p)[0] = npx / np;
		nMat.at<Vec2f>(p)[1] = npy / np;
	}

}

void calc_Cp_Dp_pmax(int sz, vector<vector<Point> > cnts, Mat mask, Mat img_gray, Mat &Cp, Mat &Dp, Mat &Pp, Mat &nMat, Mat &cnt_grad_x,
	Mat &cnt_grad_y, Mat &n_check, Mat &grad_check, Point &pmax, bool debuging)
{
	int r = mask.rows;
	int c = mask.cols;
	bool logsEnabled = false;
	ofstream CpLog;
	ofstream IpLog;
	ofstream NpLog;
	ofstream DpLog;
	ofstream PpLog;
	if (logsEnabled)
	{
		CpLog.open("CpLog.txt");
		IpLog.open("IpLog.txt");
		NpLog.open("NpLog.txt");
		DpLog.open("DpLog.txt");
		PpLog.open("PpLog.txt");
	}

	//extended mask
	int r1 = r + 4;
	int c1 = c + 4;

	Mat ext_mask(r1, c1, CV_8U, Scalar(255));

	mask.copyTo(ext_mask(Rect(2, 2, c, r)));  //copy mask to extended mask -- makes border pixels 255


	float Cp_pmax = 0.0;
	Mat temp_img;
	Mat temp_out = Mat::zeros(r, c, CV_32FC3);
	img_gray.convertTo(temp_img, CV_32F);

	int a1 = 1;   //3 for scharr  1 for sobel
	int a2 = 2;   // 10 for scharr 2 for sobel
	float max_priority = -1;
	int a = (sz - 1) / 2;
	int count = 0;

	for (int j = 0; j < cnts.size(); j++)   // at one particular contour
	{
		for (int i = 0; i < cnts[j].size(); i++)  // at each point on contour j
		{
			float Ip_val = -1000000.0;
			float Ip_Grad = 0.0;
			int Ip_coord_x = 0;
			int Ip_coord_y = 0;

			Point p = Point(cnts[j][i].x, cnts[j][i].y);
			Point pk = Point(p.x - a, p.y - a);

			// compute gradient for patch at p
			float grad = -1;
			float Ipx = 0.0;
			float Ipy = 0.0;
			float Ip_Grad_px = 0.0;
			float Ip_Grad_py = 0.0;

			float sum_Cpp = 0;
			float App = 0;

			for (int k = 0; k < sz; k++)
			{
				for (int t = 0; t < sz; t++)
				{
					Point pt = Point(pk.x + k, pk.y + t);

					if (pt.x < 0 || pt.x > c - 1 || pt.y < 0 || pt.y > r - 1)
						continue;

					sum_Cpp += Cp.at<float>(pt);
					App++;

					if (mask.at<uchar>(pt) == 0)
						continue;

					Rect roi_pt = Rect(pt.x - 1, pt.y - 1, 3, 3);

					if (pt.x - 1 < 0 || pt.x + 1 > c - 1 || pt.y - 1 < 0 || pt.y + 1 > r - 1)
						continue;


					// sobel kernel contains a masked pixel -- dont consider
					if ((int)sum(mask(roi_pt))[0] != 9 * 255)
					{
						//IpLog << p.x << "," << p.y << "," << pt.x << "," << pt.y << "," << "0" << "," << "0" << endl;
						continue;
					}

					float p11 = temp_img.at<float>(pt.y - 1, pt.x - 1);
					float p12 = temp_img.at<float>(pt.y - 1, pt.x);
					float p13 = temp_img.at<float>(pt.y - 1, pt.x + 1);
					float p21 = temp_img.at<float>(pt.y, pt.x - 1);
					float p23 = temp_img.at<float>(pt.y, pt.x + 1);
					float p31 = temp_img.at<float>(pt.y + 1, pt.x - 1);
					float p32 = temp_img.at<float>(pt.y + 1, pt.x);
					float p33 = temp_img.at<float>(pt.y + 1, pt.x + 1);

					float grad_px, grad_py;

					//apply sobel/scharr filter at p
					grad_px = a1*p13 + a2*p23 + a1*p33 - a1*p11 - a2*p21 - a1*p31;
					grad_py = a1*p31 + a2*p32 + a1*p33 - a1*p11 - a2*p12 - a1*p13;

					grad_check.at<uchar>(pt) = 255; // update gradient check status

					cnt_grad_x.at<float>(pt) = grad_px;
					cnt_grad_y.at<float>(pt) = grad_py;

					float tmp_grad = sqrt(grad_px*grad_px + grad_py*grad_py);
					Ip_Grad = tmp_grad;

					if (Ip_val < Ip_Grad) // keep the max gradient magnitude and anchor position
					{
						Ip_val = Ip_Grad;
						Ip_coord_x = p.x;
						Ip_coord_y = p.y;
						Ip_Grad_px = grad_px;
						Ip_Grad_py = grad_py;
					}
				}
			}


			if (logsEnabled)
				IpLog << Ip_coord_x << "," << Ip_coord_y << "," << Ip_Grad_px << "," << Ip_Grad_py << endl;

			Mat mask_n = ext_mask(Rect(p.x, p.y, 5, 5));
			compNbySobel(mask_n, p, 2, nMat);
			if (logsEnabled)
				NpLog << p.x << "," << p.y << "," << nMat.at<Vec2f>(p)[0] << "," << nMat.at<Vec2f>(p)[1] << endl;

			n_check.at<uchar>(p) = 255;
			float Cp_val = sum_Cpp / App;//sum_p/Ap;  // value of Cp as per paper
			if (logsEnabled)
				CpLog << p.x << ", " << p.y << "," << Cp_val << endl;

			float alpha = 255;
			float Dp_val = -1;
			int Dp_coord_x = 0;
			int Dp_coord_y = 0;

			for (int ix = 0; ix < sz; ix++)
			{
				for (int jy = 0; jy < sz; jy++)
				{
					Point pp = Point(pk.x + ix, pk.y + jy);

					if (mask.at<uchar>(pp) == 0)
						continue;

					if (pp.x < 0 || pp.x > c - 1 || pp.y < 0 || pp.y > r - 1)
						continue;

					float Ipx = cnt_grad_y.at<float>(pp.y, pp.x);
					float Ipy = -1 * cnt_grad_x.at<float>(pp.y, pp.x);

					float mag_n = sqrt((nMat.at<Vec2f>(p)[0] * nMat.at<Vec2f>(p)[0]) + (nMat.at<Vec2f>(p)[1] * nMat.at<Vec2f>(p)[1]));
					float mag_Ip = sqrt((Ipx*Ipx) + (Ipy*Ipy));

					float dp = ((abs(nMat.at<Vec2f>(p)[0] * Ipx + nMat.at<Vec2f>(p)[1] * Ipy))*mag_Ip) / alpha;

					if (Dp_val < dp)
					{
						Dp_val = dp;
						Dp_coord_x = p.x;
						Dp_coord_y = p.y;
					}
				}
			}

			if (logsEnabled)
				DpLog << Dp_coord_x << "," << Dp_coord_y << "," << Dp_val << endl;

			float Pp_val = Dp_val*Cp_val;

			if (logsEnabled)
				PpLog << p.x << "," << p.y << "," << Pp_val << endl;

			if (max_priority < Pp_val)
			{
				max_priority = Pp_val;
				pmax.x = p.x;
				pmax.y = p.y;
				Cp_pmax = Cp_val;
				count++;
			}
		}
	}

	if (count == 0)
		pmax = Point(cnts[0][0].x, cnts[0][0].y);

	Cp.at<float>(pmax) = Cp_pmax;
	if (logsEnabled)
	{
		CpLog.close();
		IpLog.close();
		NpLog.close();
		DpLog.close();
		PpLog.close();
	}
	if (debuging == true)
	{
		cout << "point of max. priority: " << pmax.x << ", " << pmax.y << endl;
		cout << "max. priority value: " << max_priority << endl;
	}
}

void replacePatchpmax(int sz, Point pmax, Point q_best_match, Mat &img_rgb, Mat &mask, Mat &Cp, int &countNZ, Mat &grad_chk, Mat &n_chk)
{
	int a = (sz - 1) / 2;//(int)floor((double)sz / 2);

	int r = mask.rows;
	int c = mask.cols;

	for (int k = -1 * a; k < a + 1; k++) //x
	{
		for (int t = -1 * a; t < a + 1; t++) //y
		{
			if (pmax.y + t > r - 1 || pmax.y + t < 0 || pmax.x + k > c - 1 || pmax.x + k < 0)
				continue;

			if (q_best_match.y + t > r - 1 || q_best_match.y + t < 0 || q_best_match.x + k > c - 1 || q_best_match.x + k < 0)
				continue;

			//float mask_Pix_Val = ;
			if (mask.at<uchar>(pmax.y + t, pmax.x + k) == 0 && mask.at<uchar>(q_best_match.y + t, q_best_match.x + k) != 0)
			{
				img_rgb.at<Vec3b>(pmax.y + t, pmax.x + k)[0] = img_rgb.at<Vec3b>(q_best_match.y + t, q_best_match.x + k)[0];
				img_rgb.at<Vec3b>(pmax.y + t, pmax.x + k)[1] = img_rgb.at<Vec3b>(q_best_match.y + t, q_best_match.x + k)[1];
				img_rgb.at<Vec3b>(pmax.y + t, pmax.x + k)[2] = img_rgb.at<Vec3b>(q_best_match.y + t, q_best_match.x + k)[2];
				mask.at<uchar>(pmax.y + t, pmax.x + k) = 255;
				grad_chk.at<uchar>(pmax.y + t, pmax.x + k) = 255;
				n_chk.at<uchar>(pmax.y + t, pmax.x + k) = 255;
				Cp.at<float>(pmax.y + t, pmax.x + k) = Cp.at<float>(pmax.y, pmax.x);
				countNZ++;
			}
		}
	}
}

void getSmoothContour(Mat mask, vector<vector<Point> > &cnts)
{

	int r = mask.rows;
	int c = mask.cols;

	int b = 3;

	int r1 = r + 2 * b;
	int c1 = c + 2 * b;

	//imshow("mask", mask);

	Mat extendedMask(r1, c1, CV_8U, Scalar(0));

	Rect roi = Rect(b, b, c, r);
	mask.copyTo(extendedMask(roi));  //copy mask to extended mask -- makes border pixels 255
	cnts.clear();
	vector<vector<Point> > temp_cnts;
	vector<vector<Point> > temp_cnts2;

	findContours(extendedMask, temp_cnts, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(-1 * (b), -1 * (b)));

	for (int i = 0; i < temp_cnts.size(); i++)
	{
		double a = contourArea(temp_cnts[i], false);

		// ignore outer boundary contour
		if (a < 0.7*r*c)
			temp_cnts2.push_back(temp_cnts[i]);
	}
	smoothContours(temp_cnts2, cnts);
}



void getContour(Mat mask, vector<vector<Point> > &cnts)
{
	int r = mask.rows;
	int c = mask.cols;

	int b = 3;

	int r1 = r + 2 * b;
	int c1 = c + 2 * b;
	Mat extendedMask(r1, c1, CV_8U, Scalar(0));
	Rect roi = Rect(b, b, c, r);
	mask.copyTo(extendedMask(roi));  //copy mask to extended mask -- makes border pixels 255
	cnts.clear();

	vector<vector<Point> > temp_cnts;
	findContours(extendedMask, temp_cnts, CV_RETR_TREE, CV_CHAIN_APPROX_NONE, Point(-1 * (b), -1 * (b)));

	for (int i = 0; i < temp_cnts.size(); i++)
	{
		double a = contourArea(temp_cnts[i], false);
		// ignore outer boundary contour
		if (a < 0.7*r*c)
			cnts.push_back(temp_cnts[i]);
	}
}

int apply_inpaint(Mat input_rgb, Mat mask_original, int sz, int sf, bool debuging, Mat &output_img)
{
	int ab = (sz - 1) / 2;//(int)floor((double)sz / 2);

	int sz2 = sz;

	Mat img_rgb;
	Mat img_gray;

	int searchFactor = sf;    /*searchFactor introduced */
	if (debuging == true)
		cout << "Search factor: " << sf << endl;

	input_rgb.copyTo(img_rgb);
	cvtColor(img_rgb, img_gray, CV_RGB2GRAY);
	int r = img_rgb.rows;
	int c = img_rgb.cols;

	// convert to CIE Lab colour space
	Mat img_cielab;
	cvtColor(img_rgb, img_cielab, CV_RGB2Lab);

	Mat mask;
	mask_original.copyTo(mask); // masked region is white everywhere else its black

	//invert mask  -- masked region is filled with zeros
	threshold(mask, mask, 3, 255, CV_THRESH_BINARY);  // binary mask same with white masked region
	vector<vector<Point> > cnts_tmp;
	getContour(mask, cnts_tmp);
	Mat tempMask = Mat(r, c, CV_8U, Scalar(255));  //tempMask is white completely
	vector<vector<Point> > cnts;

	for (int i = 0; i < cnts_tmp.size(); i++)
	{
		if (contourArea(cnts_tmp[i]) > 200)   // ignoring smaller contours
		{
			drawContours(tempMask, cnts_tmp, i, Scalar(0), -1);  // black inside masked region and white outside
			cnts.push_back(cnts_tmp[i]);
		}
	}
	tempMask.copyTo(mask);
	Mat original_mask;
	mask.copyTo(original_mask);

	// initialize
	//Start of the inpainting algorithm
	Mat	priority_Pp = Mat::zeros(r, c, CV_32FC1);
	Mat	data_Dp = Mat::zeros(r, c, CV_32FC1);
	Mat	confidence_Cp = Mat::zeros(r, c, CV_8UC1);
	Mat cp_img;
	Mat result_img = Mat::zeros(r, c, CV_32FC3);

	mask.copyTo(confidence_Cp);		       // Initialise the confidence matrix to 255 except for the masked area which should be set at 0
	mask.copyTo(cp_img);
	img_rgb.copyTo(result_img, mask);	   // Set masked area to zero in the main image
	threshold(confidence_Cp, confidence_Cp, 0, 1, CV_THRESH_BINARY);
	confidence_Cp.convertTo(confidence_Cp, CV_32FC1);

	int count = 0;
	Mat mask_cnt = Mat::zeros(r, c, CV_8U);
	Mat searchMask = Mat::zeros(r, c, CV_8U);

	/* search everywhere in unmasked region */
	if (searchFactor == 0)
		original_mask.copyTo(searchMask);
	else
	{
		for (int i = 0; i < cnts.size(); i++)
			drawContours(searchMask, cnts, i, Scalar(255), searchFactor*sz);
		bitwise_and(searchMask, original_mask, searchMask);
	}


	int countNZ = countNonZero(original_mask);

	Mat cnt_grad_x = Mat::zeros(r, c, CV_32F);
	Mat grad_check = Mat::zeros(r, c, CV_8U);
	Mat n_check = Mat::zeros(r, c, CV_8U);
	Mat cnt_grad_y = Mat::zeros(r, c, CV_32F);
	Mat nMat = Mat::zeros(r, c, CV_32FC2);

	while (countNZ < r*c)
	{
		if (cnts.size() == 0)
			break;

		count++;
		if (debuging == true)
		{
			cout << "iteration - " << count << endl;
			cout << "count of zeros in mask: " << r*c - countNonZero(mask) << endl;
		}

		// calculate Cp and Dp for each point on the contour
		//cout << "Calculate cp, pmax ... " << endl;
		Point pmax;
		calc_Cp_Dp_pmax(sz, cnts, mask, img_gray, confidence_Cp, data_Dp, priority_Pp, nMat, cnt_grad_x, cnt_grad_y, n_check, grad_check, pmax, debuging);

		//find best match patch using SSD
		Point q_best_match;
		findBestMatchPatch(sz2, mask, searchMask, img_cielab, pmax, q_best_match, debuging);

		if (debuging == true)
			cout << "point of best match q: " << q_best_match.x << ", " << q_best_match.y << endl;

		if (debuging == true)
			cout << "copying patch q onto patch p ... " << endl << endl;
		replacePatchpmax(sz2, pmax, q_best_match, img_rgb, mask, confidence_Cp, countNZ, grad_check, n_check);
		if (debuging == true)
		{
			Mat temp_img_rgb = Mat::zeros(r, c, CV_8UC3);
			img_rgb.copyTo(temp_img_rgb, mask);
			rectangle(temp_img_rgb, Rect(pmax.x - ab - 1, pmax.y - ab - 1, sz + 1, sz + 1), Scalar(0, 255, 255), 1, 8);
			rectangle(temp_img_rgb, Rect(q_best_match.x - ab - 1, q_best_match.y - ab - 1, sz + 1, sz + 1), Scalar(0, 0, 255), 1, 8);

			//draw contours
			for (int i = 0; i < cnts.size(); i++)
			{
				double a = contourArea(cnts[i], false);

				// ignore small contours and image boundary contour
				if (a > 0.85*r*c)
					continue;

				drawContours(temp_img_rgb, cnts, i, Scalar(0, 255, 0));
			}

			imshow("inpainting", temp_img_rgb);
			//Mat cp;
			//confidence_Cp.convertTo(cp, CV_8UC1);
			//imshow("CP", cp_img);
			//imshow("mask", mask);
			//imshow("CIE LAB", img_cielab);
			//imshow("n check", n_check);
			//imshow("grad check", grad_check);
			char k = waitKey(1);

			if (k == 'q')
				return -4;
		}

		// replace pixels of mask within patch of max. priority with corresponding best matching patch pixels
		img_rgb.convertTo(output_img, CV_8UC3);

		//update gray and cielab images
		cvtColor(img_rgb, img_gray, CV_RGB2GRAY);
		cvtColor(img_rgb, img_cielab, CV_RGB2Lab);
		threshold(mask, tempMask, 0, 255, CV_THRESH_BINARY_INV);
		getContour(tempMask, cnts);

		for (int i = 0; i < cnts.size(); i++)
			drawContours(confidence_Cp, cnts, i, Scalar(0), -1);
	}
	return 1;
}

