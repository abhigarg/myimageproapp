#include <jni.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <vector>
#include <android/log.h>

#include <sstream>
#include <iostream>
#include <string>
#include <stdio.h>



#define LOG_TAG "Jni_Part"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG,__VA_ARGS__)
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

#include "inpainting.h"

using namespace std;
using namespace cv;

extern "C"
{
//for applying grabcut: com.abggcv.imgproapp
JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyGrabcut(JNIEnv *env, jobject,
                                                                           jlong addrRgba, jlong addrMask, jlong addrForeground, jlong maskout,
                                                                           int minX, int minY, int maxX, int maxY, int reszFactor, int iter);

/*JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyInPaint(JNIEnv *env, jobject,
                                                                             jlong addrRgba, jlong addrMask,
                                                                             jlong addrInpaintOutImg, int searchFactor,
                                                                             int templarSz, int reszFactor);*/
JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyBackgroundChange(JNIEnv *env, jobject,
                                                                                      jlong addrRgba_orig, jlong addrRgba_back, jlong addrMask,
                                                                                      jlong addrOutImg, int x, int y,
                                                                                      int minX, int minY, int maxX, int maxY);

JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyGrabcut(JNIEnv *env, jobject,
                                                                           jlong addrRgba, jlong addrMask, jlong addrForeground, jlong maskout,
                                                                           int minX, int minY, int maxX, int maxY, int reszFactor, int iter) {
    LOGD("grabcut starts");

    Mat &image = *(Mat *) addrRgba;  //input RGBA image

    Mat &mask = *(Mat *) addrMask; //input mask

    Mat &foreground = *(Mat *) addrForeground;  //output

    Mat &mask_out = *(Mat* ) maskout;
/*
    if (minX < 0 || minX == image.cols)
        minX = 0;

    if (minY < 0 || minY == image.rows)
        minY = 0;

    if (maxX > image.cols || maxX == 0)
        maxX = image.cols;

    if (maxY > image.rows || maxY == 0)
        maxY = image.rows;

*/
    Mat img;
    cvtColor(image, img, CV_RGBA2BGR);

    Rect rect = Rect(Point((int) (minX / reszFactor), (int) (minY / reszFactor)),
                     Point((int) (maxX / reszFactor), (int) (maxY / reszFactor)));

    LOGI("Rectangle corners are: (%d, %d) & (%d, %d)", minX, minY, maxX, maxY);

    //rectangle(image, rect, (0, 255, 0, 255), 3);

    Mat img_resz, mask_resz;

    resize(img, img_resz, Size(img.cols / reszFactor, img.rows / reszFactor), 0, 0, CV_INTER_AREA);
    //resize(mask, mask_resz, Size(img.cols / 5, img.rows / 5), 0, 0, CV_INTER_AREA);

    //threshold(mask_resz, mask_resz, 0, 3, CV_THRESH_BINARY);

    LOGI("Size of img_resz(numRows, numCols): %dx%d", img_resz.rows, img_resz.cols);
    //LOGI("No of channels in img: %d", img.channels());

    //write image to file
    //LOGI("Writing image to file: ");
    //imwrite(stdFileName, img);



    //LOGI("No of channels in mask: %d",mask.channels());

    LOGI("Size of mask_resz: %dx%d ", mask.rows, mask.cols);


    Mat bgModel, fgModel;

    if(iter == 0)
        grabCut(img_resz, mask, rect, bgModel, fgModel, 1);
    else
        grabCut(img_resz, mask, rect, bgModel, fgModel, 1, GC_INIT_WITH_MASK);

    LOGD("Grabcut applied successfully");

    Mat temp_mask1, temp_mask2;
    compare(mask, GC_PR_FGD, temp_mask1, CMP_EQ);
    compare(mask, GC_FGD, temp_mask2, CMP_EQ);
    bitwise_or(temp_mask1, temp_mask2, mask_out);

    //resize to increased image size
    resize(mask_out, mask_resz, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

    threshold(mask_resz, mask_resz, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);

    //mask_resz.copyTo(mask_out);

    //grabcut output with background grayed
    Mat gray_img;
    cvtColor(img, gray_img, CV_BGR2GRAY);
    cvtColor(gray_img, foreground, CV_GRAY2BGR);

    img.copyTo(foreground, mask_resz);

    return 1;

}

JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyBackgroundChange(JNIEnv *env, jobject,
                                                                                      jlong addrRgba_orig, jlong addrRgba_back, jlong addrMask,
                                                                                      jlong addrOutImg, int x, int y,
                                                                                      int minX, int minY, int maxX, int maxY) {
    LOGD("background change starts");

    Mat &image_orig = *(Mat *) addrRgba_orig;  //original RGBA image
    Mat &image_back = *(Mat *) addrRgba_back;  //back RGBA image
    Mat &mask = *(Mat *) addrMask; //input mask
    Mat &out_img = *(Mat *) addrOutImg;  //output
    //Mat &mask_out = *(Mat* ) maskout;

    LOGD("Size of orig image: %d x %d", image_orig.rows, image_orig.cols);

    LOGD("Size of background image: %d x %d", image_back.rows, image_back.cols);

    LOGD("Size of mask: %d x %d", mask.rows, mask.cols);

    //LOGD("Size of output image:")

    Mat img_orig, img_back;
    cvtColor(image_orig, img_orig, CV_RGBA2BGR);
    cvtColor(image_back, img_back, CV_RGBA2BGR);

   // Point pt = Point(x, y);

    int rect_w = maxX - minX;
    int rect_h = maxY - minY;

    int rect_x = x - (int) rect_w/2;
    int rect_y = y - (int) rect_h/2;

    if(rect_x < 0)
        rect_x = 0;

    if(rect_y < 0)
        rect_y = 0;


    if(rect_x + rect_w > img_orig.cols)
        rect_w = img_orig.cols - rect_x;

    if(rect_y + rect_h > img_orig.rows)
        rect_h = img_orig.rows - rect_y;

    LOGI("rect_x:%d, rect_y:%d, rect_w:%d, rect_h:%d", rect_x, rect_y, rect_w, rect_h);
    LOGI("minX:%d, minY:%d, maxX:%d, maxY:%d", minX, minY, maxX, maxY);

    //Point rect_pt = new Point((int)((minX+maxX))/2, (int)((minY+maxY))/2);
    Rect roi_orig = Rect(Point(minX, minY), Point(maxX, maxY));

    LOGD("roi_orig created");

    Rect roi_back = Rect(rect_x, rect_y, rect_w, rect_h);

    LOGD("roi_back created");

    Mat temp_roi_orig(rect_h, rect_w, image_orig.type()), temp_roi_back(rect_h, rect_w, image_back.type());

    LOGD("temp_roi_orig created");

    LOGD("temp_roi_back created");

    Mat temp_roi_mask(rect_h, rect_w, mask.type());

    LOGD("temp_roi_mask created");

    //resize mask to orig image size:
    Mat mask_resz;

    resize(mask, mask_resz, Size(image_orig.cols, image_orig.rows), 0, 0, CV_INTER_AREA);

    threshold(mask_resz, mask_resz, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);

    mask_resz(roi_orig).copyTo(temp_roi_mask);

    LOGD("roi_orig part of mask is copied to temp_roi_mask");

    image_back.copyTo(out_img);

    LOGD("background image is copied to out_img");

    image_orig(roi_orig).copyTo(out_img(roi_back), temp_roi_mask);

    LOGD("roi_orig part of image_orig is copied to roi_back part of out_img masked by temp_roi_mask");

    return 1;


}



/*
JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyInPaint(JNIEnv *env, jobject,
                                                                             jlong addrRgba, jlong addrMask,
                                                                             jlong addrInpaintOutImg, int searchFactor,
                                                                             int templarSz, int reszFactor){

    LOGD("grabcut starts");

    Mat &image = *(Mat *) addrRgba;  //input RGBA image

    Mat &mask = *(Mat *) addrMask; //input mask

    Mat &img_out = *(Mat *) addrInpaintOutImg;  //output

    Mat temp_inpaint_out_img, inpaint_out_img_resz;

    Mat img_resz, mask_resz;

    LOGD("Channels in image: %d", image.channels());

    Mat img;
    cvtColor(image, img, CV_RGBA2BGR);

    resize(img, img_resz, Size(img.cols / reszFactor, img.rows / reszFactor), 0, 0, CV_INTER_AREA);

    apply_inpaint(img_resz, mask, templarSz, searchFactor, false, temp_inpaint_out_img);

    resize(temp_inpaint_out_img, inpaint_out_img_resz, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

    //resize to increased image size
    resize(mask, mask_resz, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

    threshold(mask_resz, mask_resz, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);

    img.copyTo(img_out);
    inpaint_out_img_resz.copyTo(img_out, mask_resz);

    return 1;

}
*/

}