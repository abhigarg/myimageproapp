#include <jni.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/opencv.hpp>
#include <vector>
#include <android/log.h>

#include <sstream>
#include <iostream>
#include <string>
#include <stdio.h>



#define LOG_TAG "Jni_Inpainting"
#define LOGI(...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG,__VA_ARGS__)
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__))

#include "inpainting.h"

using namespace std;
using namespace cv;

extern "C"
{
JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyInpaint(JNIEnv *env, jobject,
                                                                             jlong addrRgba, jlong addrMask,
                                                                             jlong addrInpaintOutImg, int searchFactor,
                                                                             int templarSz, int reszFactor, int gaussWinSz);




JNIEXPORT jint JNICALL Java_com_abggcv_myimgproapp_MainActivity_ApplyInpaint(JNIEnv *env, jobject,
                                                                             jlong addrRgba, jlong addrMask,
                                                                             jlong addrInpaintOutImg, int searchFactor,
                                                                             int templarSz, int reszFactor, int gaussWinSz) {

    LOGD("grabcut starts");

    Mat &image = *(Mat *) addrRgba;  //input RGBA image

    Mat &mask = *(Mat *) addrMask; //input mask

    Mat &img_out = *(Mat *) addrInpaintOutImg;  //output

    Mat temp_inpaint_out_img, inpaint_out_img_resz;

    Mat img_resz, mask_resz;

    LOGD("Channels in image: %d", image.channels());

    Mat img;
    cvtColor(image, img, CV_RGBA2BGR);

    resize(img, img_resz, Size(img.cols / reszFactor, img.rows / reszFactor), 0, 0, CV_INTER_AREA);

    if (gaussWinSz != 0) {
        GaussianBlur(mask, mask, Size(gaussWinSz, gaussWinSz), 0, 0);
        threshold(mask, mask, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);
    }

    //resize to increased image size
    resize(mask, mask_resz, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

    threshold(mask_resz, mask_resz, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);

    Mat inpaintMask;

    if (reszFactor != 5){
        resize(mask_resz, inpaintMask, Size(img.cols / reszFactor, img.rows / reszFactor), 0, 0, CV_INTER_AREA);
        threshold(inpaintMask, inpaintMask, 0, 255, CV_THRESH_BINARY + CV_THRESH_OTSU);
    }
    else
        mask.copyTo(inpaintMask);

    apply_inpaint(img_resz, inpaintMask, templarSz, searchFactor, false, temp_inpaint_out_img);

    resize(temp_inpaint_out_img, inpaint_out_img_resz, Size(img.cols, img.rows), 0, 0, CV_INTER_AREA);

    img.copyTo(img_out);
    inpaint_out_img_resz.copyTo(img_out, mask_resz);

    return 1;

}


}