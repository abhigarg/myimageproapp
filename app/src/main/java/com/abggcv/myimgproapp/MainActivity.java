package com.abggcv.myimgproapp;

/**
 * Created by ABGG on 13/12/2015.
 */

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
//import android.graphics.Rect;
import android.hardware.Camera;
import android.hardware.SensorManager;
import android.media.Image;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SubMenu;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.abggcv.myimgproapp.R;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("deprecation")
public class MainActivity extends Activity implements OnTouchListener {

    private String TAG = "DrawActivity";
    public String inputImageFilePath = "";
    ImageView imageView;

    Bitmap bitmapMaster;
    Canvas canvasMaster;

    Paint paintItem, paintBack, paintRect, temPaint;
    int strokeWidth = 3;
    Path pathItem, pathBack;

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 1;
    private static final double RECT_TOLERANCE = 5;

    private List<Point> ptsItem, ptsBack;
    private Point minXY, maxXY;

    //Menu
    private MenuItem[] mMenuItems;
    private SubMenu mSubMenu;

    private MenuItem[] mProcessingMenuItems;
    private SubMenu mProcessingSubMenu;

    private int gauss_win_sz = 3;
    private int searchFactor = 10;
    private int templarSz = 9;
    private int reszFactor = 5;

    private int iter = 0;

    public Mat grabcutMask, inpaintMask;
    public Mat grabcutInputImage, inpaintInputImage, inputImage, backgroundImage;

    public boolean selectItem, selectBackground, drawRect, isRect, moveRect, selTL, selTR, selBL, selBR, changeBackground;

    public Point maskPt;

    private String extStorageDirectory = Environment.getExternalStorageDirectory().getPath();
    //private String out_image_str = "";
    public String outputFolderName = extStorageDirectory + "/DCIM/ImageStudio";

    public LayoutInflater showButtons = null;
    public View showButtonsView = null;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");
                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("image_processing");

                } break;
                default:
                {
                    super.onManagerConnected(status);
                } break;
            }
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_touch_draw);

        imageView = (ImageView)findViewById(R.id.result);
        imageView.setOnTouchListener(this);

        showButtons = LayoutInflater.from(this);
        showButtonsView = showButtons.inflate(R.layout.activity_touch_draw, null);

        //Exit button
        Button buttonExit = (Button)showButtonsView.findViewById(R.id.exitbutton);
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                //getApplicationContext().startActivity(intent);
            }
        });

        //Cut button
        Button buttonCut = (Button)showButtonsView.findViewById(R.id.buttonCut);
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                applyInpaint();
            }
        });

        //Open new image
        Button buttonNewPic = (Button)showButtonsView.findViewById(R.id.buttonNewPic);
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i(TAG, " Selected choose picture");

                Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(choosePictureIntent, 1);
            }
        });

        //Copy
        Button buttonCopy = (Button)showButtonsView.findViewById(R.id.buttonCopy);
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i(TAG, " Selected GrabCut");

                if(!ptsItem.isEmpty()){
                    Log.d(TAG, "Number of foreground points: " + ptsItem.size());
                    Log.d(TAG, "Size of bitmap (Height, Width): " + bitmapMaster.getHeight() + "x" + bitmapMaster.getWidth());

                    cropItem(grabcutInputImage);
                }
            }
        });

        //Paste
        Button buttonPaste = (Button)showButtonsView.findViewById(R.id.buttonChangeBackground);
        buttonExit.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Log.i(TAG, " Selected Change Background");

                if(!inpaintMask.empty()) {
                    changeBackground = true;
                    maskPt = new Point();
                    Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(choosePictureIntent, 1);
                }
            }
        });


        selectItem = false;
        paintItem = new Paint();
        paintItem.setStyle(Paint.Style.STROKE);
        paintItem.setColor(Color.GREEN);
        paintItem.setStrokeWidth(strokeWidth);

        selectBackground = false;
        paintBack = new Paint();
        paintBack.setStyle(Paint.Style.STROKE);
        paintBack.setColor(Color.RED);
        paintBack.setStrokeWidth(strokeWidth);

        drawRect = true;
        isRect = false;
        paintRect = new Paint();
        paintRect.setStyle(Paint.Style.STROKE);
        paintRect.setColor(Color.BLUE);
        paintRect.setStrokeWidth(strokeWidth);

        temPaint = new Paint();
        temPaint.setStyle(Paint.Style.STROKE);
        temPaint.setColor(Color.MAGENTA);
        temPaint.setStrokeWidth(strokeWidth);

        moveRect = false;

        changeBackground = false;

        pathItem = new Path();
        pathBack = new Path();

        //check for output folder

        File file = new File(outputFolderName);
        if (!file.exists()) {
            file.mkdirs();
            Toast.makeText(getBaseContext(), "Image Studio directory not found. Creating new directory", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onResume(){
        super.onResume();
        if (!OpenCVLoader.initDebug()) {
            Log.d(TAG, "Internal OpenCV library not found. Using OpenCV Manager for initialization");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_11, this, mLoaderCallback);
        } else {
            Log.d(TAG, "OpenCV library found inside package. Using it!");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }

    }

    public boolean onCreateOptionsMenu(Menu menu) {

        Log.e(TAG, "Menu Created");
        mSubMenu = menu.addSubMenu("Choose");
        mProcessingSubMenu = menu.addSubMenu("Processing");

        mMenuItems = new MenuItem[6];
        //mMenuItems[0] = mSubMenu.add()
        mMenuItems[0] = mSubMenu.add(1, 0, Menu.NONE, "Choose Picture");
        mMenuItems[1] = mSubMenu.add(1, 1, Menu.NONE, "Clear Drawing");
        mMenuItems[2] = mSubMenu.add(1, 2, Menu.NONE, "Choose Stroke Width");
        mMenuItems[3] = mSubMenu.add(1, 3, Menu.NONE, "Draw Rectangle");
        mMenuItems[4] = mSubMenu.add(1, 4, Menu.NONE, "Select Crop Item");
        mMenuItems[5] = mSubMenu.add(1, 5, Menu.NONE, "Select Background");

        mProcessingMenuItems = new MenuItem[6];
        mProcessingMenuItems[0] = mProcessingSubMenu.add(2, 0, Menu.NONE, "Crop Item");
        mProcessingMenuItems[1] = mProcessingSubMenu.add(2, 1, Menu.NONE, "Remove Item");
        mProcessingMenuItems[2] = mProcessingSubMenu.add(2, 2, Menu.NONE, "Inpainting Parameters");
        mProcessingMenuItems[3] = mProcessingSubMenu.add(2, 3, Menu.NONE, "Inpainting Extra Parameters");
        mProcessingMenuItems[4] = mProcessingSubMenu.add(2, 4, Menu.NONE, "Change Background");
        mProcessingMenuItems[5] = mProcessingSubMenu.add(2, 5, Menu.NONE, "Save Output");

        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.i(TAG, "Menu option selected");

        //int id = item.getItemId();
        if (item.getGroupId() == 1) {

            if (item.getItemId() == 0) {

                Log.i(TAG, " Selected choose picture");

                Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(choosePictureIntent, 1);
            }

            if (item.getItemId() == 1) {
                Log.i(TAG, " Selected clear drawing");
                if (!pathItem.isEmpty()) {
                    pathItem.reset();
                    ptsItem.clear();
                }

                if (!ptsBack.isEmpty()) {
                    pathBack.reset();
                    ptsBack.clear();
                }

                minXY.x = bitmapMaster.getWidth();
                minXY.y = bitmapMaster.getHeight();

                maxXY.x = 0;
                maxXY.y = 0;

                imageView.setImageBitmap(null);
                bitmapMaster.recycle();

                bitmapMaster = Bitmap.createBitmap(inputImage.cols(), inputImage.rows(), Bitmap.Config.ARGB_8888);
                Utils.matToBitmap(inputImage, bitmapMaster);

                canvasMaster = new Canvas(bitmapMaster);
                canvasMaster.drawBitmap(bitmapMaster, new Matrix(), null);

                imageView.setImageBitmap(bitmapMaster);
                imageView.invalidate();

                iter = 0;
                //set everything to BGD
                grabcutMask = new Mat((int) (inputImage.rows() / reszFactor), (int) (inputImage.cols() / reszFactor), CvType.CV_8UC1, new Scalar(0));

                drawRect = true;
                moveRect = false;
                isRect = false;
                selectItem = false;
                selectBackground = false;
            }

            if(item.getItemId() == 2)
                showInputDialogStrokeWidth();

            if(item.getItemId() == 3){ //Draw rectangle
                if(!changeBackground) {
                    if (!moveRect && !isRect) {
                        drawRect = true;
                        selectBackground = false;
                        selectItem = false;
                        paintRect.setStrokeWidth(strokeWidth);
                        minXY.x = maxXY.x;
                        minXY.y = maxXY.y;
                        maxXY.x = 0;
                        maxXY.y = 0;
                    }
                }
            }

            if(item.getItemId() == 4) { //select foreground
                if(!changeBackground) {
                    if (isRect) {
                        drawRect = false;
                        moveRect = false;
                        selectBackground = false;
                        selectItem = true;
                        paintItem.setStrokeWidth(strokeWidth);
                    } else
                        Toast.makeText(this, "Draw rectangle first", Toast.LENGTH_SHORT).show();
                }
            }

            if(item.getItemId() == 5) { //select background
                if (!changeBackground) {
                    if (isRect) {
                        drawRect = false;
                        moveRect = false;
                        selectBackground = true;
                        selectItem = false;
                        paintBack.setStrokeWidth(strokeWidth);
                    } else
                        Toast.makeText(this, "Draw rectangle first", Toast.LENGTH_SHORT).show();
                }
            }
        }

        //image processing menu
        if(item.getGroupId() == 2){
            //apply grabcut
            if(item.getItemId() == 0) {
                if(ptsItem.isEmpty())
                    Toast.makeText(this, "No object/background points selected. Please touch to select some object points",
                            Toast.LENGTH_SHORT).show();
                else {
                    Log.d(TAG, "Number of foreground points: " + ptsItem.size());
                    Log.d(TAG, "Size of bitmap (Height, Width): " + bitmapMaster.getHeight() + "x" + bitmapMaster.getWidth());

                    cropItem(grabcutInputImage);
                }
            }

            //apply inpainting
            if(item.getItemId() == 1){
                applyInpaint();
            }

            //set inpainting parameters
            if(item.getItemId() == 2)
                showInputDialogInpainting();

            //set inpainting extra parameters
            if(item.getItemId() == 3)
                showInputDialogExtraInpainting();

            //change background
            if(item.getItemId() == 4){
                if(!inpaintMask.empty()) {
                    changeBackground = true;
                    maskPt = new Point();
                    Toast.makeText(this, "Select background pic", Toast.LENGTH_SHORT).show();

                    Intent choosePictureIntent = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(choosePictureIntent, 1);
                }
            }

            //show Grabcut Mask
            /*if(item.getItemId() == 5) {
                if(!grabcutMask.empty()) {
                    Log.d(TAG, "size of grabcutMask:" + grabcutMask.rows() + " x " + grabcutMask.cols());

                    loadMatToBitmap(grabcutMask);
                }
                else
                    Toast.makeText(this, "Mask is empty", Toast.LENGTH_SHORT).show();
            }*/

            //save output to file
            if(item.getItemId() == 5){

               if(bitmapMaster != null)
               {
                   showOutputDialogImageSave();

               }


            }
        }

        return true;
    }

    public void applyInpaint(){
        if(!inpaintMask.empty()){
            Mat inpaint_out = new Mat(inpaintInputImage.rows(), inpaintInputImage.cols(), CvType.CV_8UC3);
            Mat inpaint_in = new Mat(inpaintInputImage.size(), CvType.CV_8UC4);
            inpaintInputImage.copyTo(inpaint_in);

            Mat inpaint_mask = new Mat(inpaintMask.size(), CvType.CV_8UC1);
            inpaintMask.copyTo(inpaint_mask);

            ApplyInpaint(inpaint_in.getNativeObjAddr(), inpaint_mask.getNativeObjAddr(), inpaint_out.getNativeObjAddr(),
                    searchFactor, templarSz, reszFactor, gauss_win_sz);

            Toast.makeText(this, "Item removal completed", Toast.LENGTH_SHORT).show();

            Bitmap tempBitmap = Bitmap.createBitmap(bitmapMaster.getWidth(), bitmapMaster.getHeight(), bitmapMaster.getConfig());

            if (bitmapMaster != null) {
                imageView.setImageBitmap(null);
                bitmapMaster.recycle();
            }

            Mat inPaintImage = new Mat(inpaint_out.rows(), inpaint_out.cols(), CvType.CV_8UC4);

            Imgproc.cvtColor(inpaint_out, inPaintImage, Imgproc.COLOR_BGR2RGBA);

            bitmapMaster = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), tempBitmap.getConfig());

            Utils.matToBitmap(inPaintImage, bitmapMaster);

            canvasMaster = new Canvas(bitmapMaster);
            canvasMaster.drawBitmap(bitmapMaster, new Matrix(), null);
            imageView.setImageBitmap(bitmapMaster);
        }
        else
            Toast.makeText(this, "No crop item selected to remove. First select crop item", Toast.LENGTH_SHORT).show();
    }


    protected void onActivityResult(int requestCode, int resultCode,
                                    Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Bitmap tempBitmap;

        if (requestCode == 1 && resultCode == RESULT_OK && null!= data) {
            Uri imageFileUri = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(imageFileUri, null, null, null, null);
            if (cursor == null) { // Source is Dropbox or other similar local file path
                inputImageFilePath = imageFileUri.getPath();
            } else {
                cursor.moveToFirst();
                int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                inputImageFilePath = cursor.getString(idx);
                cursor.close();
            }

            Log.i(TAG,"Selected image file path: " + inputImageFilePath);

            try {
                BitmapFactory.Options bmpFactoryOptions = new BitmapFactory.Options();
                bmpFactoryOptions.inJustDecodeBounds = true;
                tempBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        imageFileUri), null, bmpFactoryOptions);

                bmpFactoryOptions.inJustDecodeBounds = false;
                tempBitmap = BitmapFactory.decodeStream(getContentResolver().openInputStream(
                        imageFileUri), null, bmpFactoryOptions);

                Bitmap.Config config;
                if(tempBitmap.getConfig() != null){
                    config = tempBitmap.getConfig();
                }else{
                    config = Bitmap.Config.ARGB_8888;
                }

                if(bitmapMaster!=null) {
                    imageView.setImageBitmap(null);
                    bitmapMaster.recycle();
                }

                //bitmapMaster is Mutable bitmap
                bitmapMaster = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), config);
                //tempBitmap.recycle();

                Matrix matrix = new Matrix();

                ptsItem = new ArrayList<Point>();
                ptsBack = new ArrayList<Point>();

                canvasMaster = new Canvas(bitmapMaster);
                canvasMaster.drawBitmap(tempBitmap, matrix, null);
                imageView.setImageBitmap(bitmapMaster);

                if(!changeBackground) {
                    minXY = new Point();
                    maxXY = new Point();

                    minXY.x = tempBitmap.getWidth();
                    minXY.y = tempBitmap.getHeight();

                    maxXY.x = 0;
                    maxXY.y = 0;

                    iter = 0;
                    inpaintMask = new Mat();
                    grabcutInputImage = new Mat();
                    inpaintInputImage = new Mat();
                    inputImage = new Mat();

                    Utils.bitmapToMat(tempBitmap, inputImage);
                    //inputImage = Highgui.imread(inputImageFilePath);
                    inputImage.copyTo(grabcutInputImage);
                    inputImage.copyTo(inpaintInputImage);

                    grabcutMask = new Mat(inputImage.rows() / reszFactor, inputImage.cols() / reszFactor, CvType.CV_8UC1, new Scalar(0));

                    drawRect = true;
                    moveRect = false;
                    isRect = false;
                    selectItem = false;
                    selectBackground = false;
                }
                else {
                    backgroundImage = new Mat();
                    //backgroundImage = Highgui.imread(inputImageFilePath);
                    Utils.bitmapToMat(tempBitmap, backgroundImage);
                    Toast.makeText(this, "Touch to paste cropped item", Toast.LENGTH_SHORT).show();
                }

                tempBitmap.recycle();

            } catch (Exception e) {
                Log.v("ERROR", e.toString());
            }
        }
    }


    //@Override
    public boolean onTouch(View v, MotionEvent event) {
        float x = event.getX();
        float y = event.getY();

        ImageView iv = (ImageView) v;

        if(x < 0) x = 0; if(x > iv.getWidth()) x = iv.getWidth();
        if(y < 0) y = 0; if(y > iv.getHeight()) y = iv.getHeight();

        /*if(x < 0 || x > iv.getWidth() || y < 0 || y > iv.getHeight()) {
            Toast.makeText(this, "Touch outside screen bounds", Toast.LENGTH_SHORT).show();
            return true;
        }*/

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if(!changeBackground) {
                    if (!moveRect) {
                        touch_start(iv, bitmapMaster, x, y);
                        imageView.invalidate();
                    } else {
                        selTL = false;
                        selTR = false;
                        selBR = false;
                        selBL = false;

                        mX = x * (float) bitmapMaster.getWidth() / (float) iv.getWidth();
                        mY = y * (float) bitmapMaster.getHeight() / (float) iv.getHeight();

                        double dtl = (minXY.x - mX) * (minXY.x - mX) + (minXY.y - mY) * (minXY.y - mY);
                        double dtr = (maxXY.x - mX) * (maxXY.x - mX) + (minXY.y - mY) * (minXY.y - mY);
                        double dbl = (minXY.x - mX) * (minXY.x - mX) + (maxXY.y - mY) * (maxXY.y - mY);
                        double dbr = (maxXY.x - mX) * (maxXY.x - mX) + (maxXY.y - mY) * (maxXY.y - mY);

                        double dmin = Math.min(Math.min(dtl, dtr), Math.min(dbl, dbr));

                        String toast_str = "";

                        if (dmin < RECT_TOLERANCE) {
                            if (dmin == dbl) {
                                selBL = true;
                                toast_str = "BL";
                            }
                            if (dmin == dbr) {
                                selBR = true;
                                toast_str = "BR";
                            }
                            if (dmin == dtl) {
                                selTL = true;
                                toast_str = "TL";
                            }
                            if (dmin == dtr) {
                                selTR = true;
                                toast_str = "TR";
                            }

                            Toast.makeText(this, "Corner selected: " + toast_str, Toast.LENGTH_SHORT).show();
                        } else
                            Toast.makeText(this, "Distance from nearest point: " + dmin, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    //processing for changing background
                    Mat copyMask = new Mat();
                    inpaintMask.copyTo(copyMask);

                    Log.d(TAG, "size of inpaint mask: " + inpaintMask.cols() + " x " + inpaintMask.rows());

                    Mat copyBack = new Mat();
                    backgroundImage.copyTo(copyBack);

                    Log.d(TAG, "size of background image: " + backgroundImage.cols() + " x " + backgroundImage.rows());

                    Mat copyOrig = new Mat();
                    inputImage.copyTo(copyOrig);

                    Log.d(TAG, "size of input image: " + inputImage.cols() + " x " + inputImage.rows());

                    mX = x * (float) bitmapMaster.getWidth() / (float) iv.getWidth();
                    mY = y * (float) bitmapMaster.getHeight() / (float) iv.getHeight();

                    Log.d(TAG, "Point of paste: " + mX + ", " + mY);
                    Log.d(TAG, "minX: " + minXY.x + ", minY: " + minXY.y + ", maxX: " + maxXY.x + "maxY: " + maxXY.y);

                    Mat outImg = new Mat();
                    ApplyBackgroundChange(copyOrig.getNativeObjAddr(), copyBack.getNativeObjAddr(), copyMask.getNativeObjAddr(),
                                          outImg.getNativeObjAddr(), (int)mX, (int)mY, (int)minXY.x, (int)minXY.y, (int)maxXY.x, (int)maxXY.y);

                    Toast.makeText(this, "Item removal completed", Toast.LENGTH_SHORT).show();

                    Bitmap tempBitmap = Bitmap.createBitmap(bitmapMaster.getWidth(), bitmapMaster.getHeight(), bitmapMaster.getConfig());

                    if (bitmapMaster != null) {
                        imageView.setImageBitmap(null);
                        bitmapMaster.recycle();
                    }

                    bitmapMaster = Bitmap.createBitmap(tempBitmap.getWidth(), tempBitmap.getHeight(), tempBitmap.getConfig());

                    Utils.matToBitmap(outImg, bitmapMaster);

                    canvasMaster = new Canvas(bitmapMaster);
                    canvasMaster.drawBitmap(bitmapMaster, new Matrix(), null);
                    imageView.setImageBitmap(bitmapMaster);
                }

                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(iv, bitmapMaster, x, y);
                imageView.invalidate();
                break;
            case MotionEvent.ACTION_UP:
                if(!moveRect) {
                    touch_up();
                    imageView.invalidate();
                }
                else{
                    float projectedX = x * (float) bitmapMaster.getWidth() / (float) iv.getWidth();
                    float projectedY = y * (float) bitmapMaster.getHeight() / (float) iv.getHeight();

                    if(selBL){
                        minXY.x = projectedX;
                        maxXY.y = projectedY;
                    }

                    if(selTL){
                        minXY.x = projectedX;
                        minXY.y = projectedY;
                    }

                    if(selBR){
                        maxXY.x = projectedX;
                        maxXY.y = projectedY;
                    }

                    if(selTR){
                        maxXY.x = projectedX;
                        minXY.y = projectedY;
                    }

                    //clear previous rectangle
                    imageView.setImageBitmap(null);
                    bitmapMaster.recycle();

                    bitmapMaster = Bitmap.createBitmap(inputImage.cols(), inputImage.rows(), Bitmap.Config.ARGB_8888);
                    Utils.matToBitmap(inputImage, bitmapMaster);
                    canvasMaster = new Canvas(bitmapMaster);

                    imageView.setImageBitmap(bitmapMaster);
                    imageView.invalidate();

                    canvasMaster.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRect);
                    imageView.invalidate();

                    selBL = false; selBR = false; selTL = false; selTR = false;

                    grabcutMask = new Mat(inputImage.rows()/reszFactor, inputImage.cols()/reszFactor, CvType.CV_8UC1, new Scalar(0));
                    Core.rectangle(grabcutMask, new Point(minXY.x/reszFactor, minXY.y/reszFactor), new Point(maxXY.x/reszFactor, maxXY.y/reszFactor),
                            new Scalar(3), -1);  //set rectangle area to PR_FGD
                }
                break;
        }
        return true;
    }

    private void touch_start(ImageView iv, Bitmap bm, float x, float y) {
        if(bitmapMaster == null)
            Toast.makeText(this, "Load an image first", Toast.LENGTH_SHORT).show();

        else {
            if(selectItem)
                pathItem = new Path();

            if(selectBackground)
                pathBack = new Path();

            float projectedX = x * (float) bm.getWidth() / (float) iv.getWidth();
            float projectedY = y * (float) bm.getHeight() / (float) iv.getHeight();

            if(selectItem)
                pathItem.moveTo(projectedX, projectedY);
            if(selectBackground)
                pathBack.moveTo(projectedX, projectedY);

            mX = projectedX;
            mY = projectedY;

            if(drawRect) {
                minXY.x = mX;
                minXY.y = mY;
                maxXY.x = (int)((bm.getWidth() - mX)/2);
                maxXY.y = (int)((bm.getHeight() - mY)/2);
            }
        }
    }

    private void touch_move(ImageView iv, Bitmap bm, float x, float y) {
        float projectedX = x *(float)bm.getWidth()/(float)iv.getWidth();
        float projectedY = y *(float)bm.getHeight()/(float)iv.getHeight();

        float dx = Math.abs(projectedX - mX);
        float dy = Math.abs(projectedY - mY);

        if (dx >= TOUCH_TOLERANCE || dy >= TOUCH_TOLERANCE) {
            if(selectItem)
                pathItem.quadTo(mX, mY, projectedX, projectedY);

            if(selectBackground)
                pathBack.quadTo(mX, mY, projectedX, projectedY);

            mX = projectedX;
            mY = projectedY;
        }
    }

    private void touch_up() {
        if (selectItem) {
            pathItem.lineTo(mX, mY);
            // commit the path to our offscreen
            canvasMaster.drawPath(pathItem, paintItem);

            //add points from path to arrayList of opencv points
            getItemPoints();
        }

        if (selectBackground) {
            pathBack.lineTo(mX, mY);
            // commit the path to our offscreen
            canvasMaster.drawPath(pathBack, paintBack);

            //add points from path to arrayList of opencv points
            getBackPoints();
        }

        if(drawRect) {
            if (minXY.x > mX)
                minXY.x = mX;

            if (minXY.y > mY)
                minXY.y = mY;

            if (maxXY.x < mX)
                maxXY.x = mX;

            if (maxXY.y < mY)
                maxXY.y = mY;

            Log.d(TAG, "minX: " + minXY.x + ", minY: " + minXY.y + ", maxX: " + maxXY.x + ", maxY: " + maxXY.y);

            canvasMaster.drawRect((float) minXY.x, (float) minXY.y, (float) maxXY.x, (float) maxXY.y, paintRect);
            moveRect = true;
            drawRect = false;
            isRect = true;
            Core.rectangle(grabcutMask, new Point(minXY.x/reszFactor, minXY.y/reszFactor), new Point(maxXY.x/reszFactor, maxXY.y/reszFactor),
                    new Scalar(3), -1);  //set rectangle area to PR_FGD
        }

        if(!grabcutInputImage.empty()) {
            if (!ptsBack.isEmpty() && !ptsItem.isEmpty()) {
                Mat in_img = new Mat(grabcutInputImage.rows(), grabcutInputImage.cols(), CvType.CV_8UC4);
                grabcutInputImage.copyTo(in_img);
                cropItem(in_img);
            }
            else
                Toast.makeText(this, "Choose both background and foreground points", Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(this, "No image loaded", Toast.LENGTH_SHORT).show();
    }

    //Item points to opencv array of points
    public void getItemPoints() {
        PathMeasure pm = new PathMeasure(pathItem, false);
        float length = pm.getLength();
        float distance = 0f;
        float speed = length / 20;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < 20)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);
            ptsItem.add(new Point(aCoordinates[0], aCoordinates[1]));
            grabcutMask.put((int) (aCoordinates[1]/reszFactor), (int) (aCoordinates[0]/reszFactor), (byte) (1 & 0xFF));
            counter++;
            distance = distance + speed;
        }
    }

    //Item points to opencv array of points
    public void getBackPoints() {
        PathMeasure pm = new PathMeasure(pathBack, false);
        float length = pm.getLength();
        float distance = 0f;
        float speed = length / 20;
        int counter = 0;
        float[] aCoordinates = new float[2];

        while ((distance < length) && (counter < 20)) {
            // get point from the path
            pm.getPosTan(distance, aCoordinates, null);
            ptsBack.add(new Point(aCoordinates[0], aCoordinates[1]));
            grabcutMask.put((int) (aCoordinates[1]/reszFactor), (int) (aCoordinates[0]/reszFactor), (byte) (0 & 0xFF));
            counter++;
            distance = distance + speed;
        }
    }


    public void cropItem(Mat img){
        Log.d(TAG, "Cropping Item");

        Mat fore = new Mat(img.rows(), img.cols(), CvType.CV_8UC3);

        ApplyGrabcut(img.getNativeObjAddr(), grabcutMask.getNativeObjAddr(), fore.getNativeObjAddr(), inpaintMask.getNativeObjAddr(),
                (int)minXY.x, (int)minXY.y, (int)maxXY.x, (int)maxXY.y, reszFactor, iter);

        iter = 1;

        Toast.makeText(this, "Item cropping completed", Toast.LENGTH_SHORT).show();

        if (bitmapMaster != null) {
            imageView.setImageBitmap(null);
            bitmapMaster.recycle();
        }

        Mat foreRgba = new Mat();
        Imgproc.cvtColor(fore, foreRgba, Imgproc.COLOR_BGR2RGBA);

        bitmapMaster = Bitmap.createBitmap(fore.cols(), fore.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(foreRgba, bitmapMaster);

        canvasMaster = new Canvas(bitmapMaster);
        canvasMaster.drawBitmap(bitmapMaster, new Matrix(), null);
        imageView.setImageBitmap(bitmapMaster);
    }

    public void pasteItem(Mat backImg, Point pastePt){




    }


    public void loadMatToBitmap(Mat img){

        if (bitmapMaster != null) {
            imageView.setImageBitmap(null);
            bitmapMaster.recycle();
        }

        Mat Rgba = new Mat();

        if(img.channels() == 3)
            Imgproc.cvtColor(img, Rgba, Imgproc.COLOR_BGR2RGBA);
        if(img.channels() == 1) {
            Mat Bgr = new Mat();
            Imgproc.cvtColor(img, Bgr, Imgproc.COLOR_GRAY2BGR);
            Imgproc.cvtColor(Bgr, Rgba, Imgproc.COLOR_BGR2RGBA);
        }

        if(img.channels() == 4)
            img.copyTo(Rgba);

        bitmapMaster = Bitmap.createBitmap(img.cols(), img.rows(), Bitmap.Config.ARGB_8888);

        Utils.matToBitmap(Rgba, bitmapMaster);

        canvasMaster = new Canvas(bitmapMaster);
        canvasMaster.drawBitmap(bitmapMaster, new Matrix(), null);
        imageView.setImageBitmap(bitmapMaster);

    }


    public static String getRealPathFromURI(Context context, Uri uri){
        String filePath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);

        // Split at colon, use second item in the array
        String id = wholeID.split(":")[1];

        String[] column = { MediaStore.Images.Media.DATA };

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                column, sel, new String[]{ id }, null);

        int columnIndex = cursor.getColumnIndex(column[0]);

        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }

    //Input for stroke width
    protected void showInputDialogStrokeWidth() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        Toast.makeText(MainActivity.this, "Stroke width: " + editText.getText(),Toast.LENGTH_SHORT).show();
                        strokeWidth = Integer.parseInt(editText.getText().toString()); //editText.getText();
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    //Input for inpainting
    protected void showInputDialogInpainting() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_inpainting_params_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText templarSzText = (EditText) promptView.findViewById(R.id.templar_sz_text);
        final EditText reszText = (EditText) promptView.findViewById(R.id.resize_text);
        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        //Toast.makeText(MainActivity.this, "Gaussian smoothing window size: " + gaussWinSzText.getText(),Toast.LENGTH_SHORT).show();
                        reszFactor = Integer.parseInt(reszText.getText().toString());
                        templarSz = Integer.parseInt(templarSzText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }

    //Extra inputs for inpainting
    protected void showInputDialogExtraInpainting() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.input_inpainting_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText gaussWinSzText = (EditText) promptView.findViewById(R.id.gauss_win_sz_text);
        final EditText searchFactorText = (EditText) promptView.findViewById(R.id.search_factor_text);

        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        //Toast.makeText(MainActivity.this, "Gaussian smoothing window size: " + gaussWinSzText.getText(),Toast.LENGTH_SHORT).show();
                        gauss_win_sz = Integer.parseInt(gaussWinSzText.getText().toString()); //editText.getText();
                        searchFactor = Integer.parseInt(searchFactorText.getText().toString());
                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
    }


    //Input for output image save
    protected void showOutputDialogImageSave() {
        // get prompts.xml view
        LayoutInflater layoutInflater = LayoutInflater.from(MainActivity.this);
        View promptView = layoutInflater.inflate(R.layout.output_img_save_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
        alertDialogBuilder.setView(promptView);

        final EditText outputImgText = (EditText) promptView.findViewById(R.id.output_image_text);


        // setup a dialog window
        alertDialogBuilder.setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //resultText.setText("Hello, " + editText.getText());
                        Toast.makeText(MainActivity.this, "Saving output image to /DCIM/ImageStudio/: " + outputImgText.getText() + ".jpg", Toast.LENGTH_SHORT).show();
                        String out_image_str = outputFolderName + "/" + outputImgText.getText().toString() + ".jpg"; //editText.getText();

                        Mat temp_out = new Mat();
                        Utils.bitmapToMat(bitmapMaster, temp_out);
                        Mat out_bgr = new Mat();
                        Imgproc.cvtColor(temp_out, out_bgr, Imgproc.COLOR_RGBA2BGR);
                        Highgui.imwrite(out_image_str, out_bgr);

                    }
                })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

        // create an alert dialog
        AlertDialog alert = alertDialogBuilder.create();
        alert.show();
        //Highgui.imwrite()
        //Highgui.imwrite(outputFolderName + "/" + )


    }

    @SuppressWarnings("JniMissingFunction")
    public native int ApplyBackgroundChange(long origImgAdd, long backImgAdd, long maskAdd, long outImgAdd,
                                            int x, int y, int minX, int minY, int maxX, int maxY);

    @SuppressWarnings("JniMissingFunction")
    public native int ApplyGrabcut(long imgAdd, long maskAdd, long foreAdd, long maskOutAdd, int minX,
                                   int minY, int maxX, int maxY, int reszFactor, int iter);


    @SuppressWarnings("JniMissingFunction")
    public native int ApplyInpaint(long imgAdd, long maskAddint, long inpaintOutAdd, int searchFactor, int templarSz,
                                   int reszFactor, int gaussWinSz);  //ApplyInpaint
}